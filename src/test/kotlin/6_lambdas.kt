fun foo(f: (Int) -> String) {
    println(f(42))
}

fun main(args: Array<String>) {

    // lambdy / anonimowe funkcje
    val f: (Int, Int) -> Int = { a, b ->
        a + b
    }

    f(1, 2)

    foo({ x -> "x: $x" })

    // jeśli ostatnim parametrem jest lambda:
    foo { x -> "x: $x" }

    // jeśli lambda ma tylko jeden argument:
    foo { "x: $it" }

    // możemy też przekazać referencję do metody
    foo(Int::toString)

    // lambdy z receiverem
    val g: String.() -> Unit = {
        println(reversed())
        // reversed jest funkcją na Stringu, ex. "foo".reversed() == "oof"
    }

    g("raboof")
    "foo".g()

    with("foo") {
        length
    }

    val c = conf {
        +"Kotlin"
        +"Java"
        for (i in 1..10)
            +"Scala"

        -"Python"
        -"Ruby"
    }

    assert(c == MyConfig(listOf("Kotlin", "Java", "Scala"), listOf("Python", "Ruby")))
}

fun conf(init: MyConfHelper.() -> Unit) = MyConfHelper().apply(init).run { MyConfig(whitelist, blacklist) }

data class MyConfig(val whitelist: List<String>, val blacklist: List<String> = listOf())

class MyConfHelper {
    val blacklist = mutableListOf<String>()
    val whitelist = mutableListOf<String>()

    operator fun String.unaryPlus() {
        whitelist += this
    }

    operator fun String.unaryMinus() {
        blacklist += this
    }
}
