import AST.*

sealed class AST {
    data class Num(val value: Int) : AST()
    data class Plus(val left: AST, val right: AST) : AST()
    data class Minus(val left: AST, val right: AST) : AST()
}

fun eval(ast: AST): Int = when (ast) {
    is Num -> ast.value
    is Plus -> eval(ast.left) + eval(ast.right)
    is Minus -> eval(ast.left) - eval(ast.right)
}

fun main(args: Array<String>) {
    assert(
            eval(
                    Plus(
                            Num(1),
                            Minus(Num(5), Num(2))
                    )
            ) == 4
    )
}