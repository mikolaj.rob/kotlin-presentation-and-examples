@SuppressWarnings("ALL")
public class WebServer implements AutoCloseable {
    private ServerImplKt server;

    public void init() {
        server = new ServerImplKt();
        // do some stuff
    }

    @Override
    public void close() throws Exception {
        server.shutdown();
    }
}

@SuppressWarnings("ALL")
class ServerImpl {
    public void shutdown() {}
}
