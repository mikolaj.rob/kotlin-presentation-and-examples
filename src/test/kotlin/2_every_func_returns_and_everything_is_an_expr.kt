@file:Suppress("UNUSED_VARIABLE")

fun wszystkoCośZwraca() {
    fun foo() {} // lokalne funkcje
    val x = foo()
    println("Wynik pustej funkcji: " + x)
}

fun wszystkoJestWyrażeniem(a: Int, b: Int): Foo {
    val max = if (a < b) b else a
    val mayThrow = try { add(1, 2) } catch (e: Exception) { null }

    add(1, return Foo)
}

fun main(args: Array<String>) {
    wszystkoCośZwraca()
    assert(wszystkoJestWyrażeniem(1, 2) == Foo)
}