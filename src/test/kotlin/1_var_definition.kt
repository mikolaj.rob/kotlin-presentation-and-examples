@file:Suppress("UNUSED_VARIABLE", "ASSIGNED_BUT_NEVER_ACCESSED_VARIABLE", "UNUSED_VALUE", "JoinDeclarationAndAssignment", "unused")

// spacji w nazwach funkcji używamy do czytelnego nazywania testów!
fun `ale spacji w nazwach funkcji to się nie spodziewaliście 💩`() {
    val a: Int = 1
    val b = 2
    val c: Int
    c = 3
    var d = 5
    d = 6
}