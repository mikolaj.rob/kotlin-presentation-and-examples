import kotlin.properties.Delegates
import kotlin.reflect.KProperty

// COMPOSITION OVER INHERITANCE

class MyMap(val underlyingMap: Map<String, String>) : Map<String, String> by underlyingMap {
    override val size: Int
        get() {
            println("Ktoś chce wiedzieć za dużo...")
            throw UnsupportedOperationException("Nie dam!")
        }
}

// delegowanie właściwości:
//  * leniwe właściwości
//  * obserwowanie zmian

class DelegationExample {
    val lazyExpensiveMessage by lazy {
        println("CALCULATING")
        Thread.sleep(500)
        println("DONE")
        "WE LIVE IN A SIMULATION"
    }

    var foo by Delegates.observable(1) { prop, oldValue, newValue ->
        println("Property $prop changed from $oldValue to $newValue")
    }

    var baz by MyDelegate("raBooF")
}

class MyDelegate(val x: String) {
    operator fun getValue(receiver: Any, prop: KProperty<*>): String {
        return x.reversed()
    }

    operator fun setValue(receiver: Any, prop: KProperty<*>, value: String) {
        println("trying to set value in my delegate: $value")
    }
}

fun main(args: Array<String>) {
    val x = DelegationExample()
    println(x.lazyExpensiveMessage)
    println(x.lazyExpensiveMessage)
    x.foo = 2
    x.foo = 3

    println(x.baz)
    x.baz = ":<"
    x.baz = "xD"

    fun foo(): Int {
        listOf(1, 2, 3).forEach {
            if (it == 1) return it
        }
        return 0
    }

}

inline fun <reified T> bar() {

}