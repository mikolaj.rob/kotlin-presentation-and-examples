class WebServerKt : AutoCloseable {
    private var server: ServerImplKt? = null

    fun init() {
        server = ServerImplKt()
        // do some stuff
    }

    override fun close() {
//        server.shutdown() // error
        server?.shutdown()
    }
}

private class ServerImplKt {
    fun shutdown() {}
}