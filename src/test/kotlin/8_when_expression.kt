fun main(args: Array<String>) {
    val x: Any = 10

    // when z argumentem; switch na sterydach
    when (x) {
        1 -> println("jeden")
        in 2..5 -> println("między dwa a pięć")
        is String -> println("to nie liczba, to $x")
        else -> println("jakaś duża liczba") // else jest wymagane tylko, kiedy when jest wyrażeniem (tu nie)
    }

    // when bez argumentu; jak ciąg if else if...
    when {
        Math.random() < 0.5 -> println("lucky shot")
        System.currentTimeMillis() < 315529200L -> println("witamy w latach 70.")
    }
}