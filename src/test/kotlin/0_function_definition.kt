@file:Suppress("unused", "UNUSED_VARIABLE")

fun sub(a: Long, b: Long): Int {
//    return a - b // błąd: Long to nie Int (Java doesn't care)
    return (a - b).toInt()
}

// funkcja z jedną linijką kodu - typ zwracany jest inferowany
fun add(a: Int, b: Int) = a + b

// funkcja z receiverem - extension function
// fun ReceiverType.functionName(argumentList...): ReturnType
fun Int.extensionAdd(other: Int): Int {
    // w tym scopie mamy dostępne wszystkie metody receivera
    val thisPlusOne = inc()
    return this + other
}

// funkcje infiksowe
infix fun Int.infixAdd(other: Int) = this + other

// można nawet definiować operatory jako extension functions
operator fun String.div(num: Int) = substring(0, length / num)

// lub coś bardziej przydatnego
operator fun String.invoke(f: () -> String) = "<$this>${f()}</$this>"

fun main(args: Array<String>) {
    assert(1.extensionAdd(2) == 3)
    assert(1 infixAdd 5 == 6)

    assert("foobar" / 2 == "foo")
    assert("h1" { "hello" } == "<h1>hello</h1>")
}
