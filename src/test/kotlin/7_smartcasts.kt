@file:Suppress("unused")


fun doSomeSmartCasts(x: Any?) {
    // x: Any?
    if (x is String) {
        // x: String
        println(x.substring(0, x.length-2))
    }

    if (x is Int) {
        // x: Int
        println(x.dec() + 100)
    }

    if (x != null) {
        // x: Any
        println(x.hashCode())
    }

    try {
        x as String // może rzucić ClassCastException
    } catch (ex: Exception) {}

    val y = x as? String // null jeśli x nie jest Stringiem
}